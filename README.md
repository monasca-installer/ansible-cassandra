# Apache Cassandra<sup>TM</sup>

Install [Apache Cassandra<sup>TM</sup>](http://cassandra.apache.org/) massively
scalable open source NoSQL database.

## Role Variables

| Name                                        | Default Value              | Description                                                                             |
| ----------------                            | -------------------------- | ----------------------------------------------------------------                        |
| cassandra_admin_role                        |                            | Admin role for Cassandra administration.                                                |
| cassandra_admin_password                    |                            | Password for Cassandra admin role.                                                      |
| cassandra_mon_api_role                      |                            | Cassandra role for monasca-api (read-only access).                                      |
| cassandra_mon_api_password                  |                            | Password for monasca-api role.                                                          |
| cassandra_mon_persister_role                |                            | Cassandra role for monasca-persister (read/write access).                               |
| cassandra_mon_persister_password            |                            | Password for Cassandra admin role.                                                      |
| cassandra_host                              |                            | RPC address to broadcast to drivers and other Cassandra nodes.                          |
| cassandra_port                              | 9042                       | Port on which the CQL native transport listens for clients.                             |
| cassandra_wait_for_period                   | 60                         | Maximum number of seconds to wait for Cassandra service to start.                       |
| cassandra_version                           | 3.7                        | Cassandra version to be installed.                                                      |
| cassandra_cluster_name                      | Monasca cluster            | The name of the cluster.                                                                |
| cassandra_data_dir                          | /var/opt/cassandra         | The directory location where table data (SSTables) is stored.                           |
| cassandra_rpc_address                       | 0.0.0.0                    | The listen address for client connections (Thrift remote procedure calls).              |
| cassandra_commitlog_sync                    | periodic                   | The method that Cassandra uses to acknowledge writes (periodic or batch).               |
| cassandra_commitlog_sync_period_in_ms       | 10000                      | How often the commit log is synchronized to disk.                                       |
| cassandra_commitlog_sync_batch_window_in_ms | 2                          | Control how long Cassandra waits for other writes before performing a sync.             |
| cassandra_rpc_server_type                   | sync                       | Control working mode of RPC server.                                                     |
| cassandra_rpc_address                       | 0.0.0.0                    | The address or interface to bind the Thrift RPC service and native transport server to. |
| cassandra_batch_size_warn_threshold_in_kb   | 5                          | Batch size threshold for warnings.                                                      |
| cassandra_batch_size_fail_threshold_in_kb   | 500                        | Maximum batch size.                                                                     |
| cassandra_user                              | cassandra                  | System user to run Cassandra service. Matches value installed by Cassandra rpm package. |
| cassandra_group                             | cassandra                  | System group for Cassandra files. Matches value installed by Cassandra rpm package.     |

## License

Apache License 2.0
